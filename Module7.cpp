﻿#include <iostream>
#include <math.h>
using namespace std;

class vector
{
public:
    vector()
    {}
    vector(double x, double y, double z) : x(x), y(y), z(z)
    {}
    void show()
        {
        cout << "\n" << x << " " << y << " " << z;
        }
   
    double vectorLength()
    {
        double length = (x * 2) + (y * 2) + (z * 2);
        length = sqrt(length);
        cout << "\n" << "vector length:" << length;
        return length;
    }

private:
    double x = 25;
    double y = 65;
    double z = 38;


};

int main()
{
    vector v;
    v.show();
    v.vectorLength();
}
